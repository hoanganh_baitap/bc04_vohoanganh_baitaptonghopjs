// Bài 1:
function taoBang() {
  var rows = "";

  for (var i = 1; i <= 100; i += 10) {
    var columns = "";
    for (var j = 0; j < 10; j++) {
      var column = `
          <td>
          ${i + j} 
          </td>
          `;
      columns += column;
    }
    var row = `
          <tr>
              ${columns}
          </tr>
          `;
    rows += row;
  }
  document.getElementById("table").innerHTML = rows;
}
// Bài 2
var soNguyenArr = [];
function taoMangSoNguyen() {
  var nhaSo = document.getElementById("txtSoNguyen");
  soNguyenArr.push(nhaSo.value * 1);
  document.getElementById("inSoNguyen").innerHTML = soNguyenArr;
  nhaSo.value = "";
}

function laSoNguyenTo(n) {
  if (n < 2) {
    return false;
  }

  if (n === 2) {
    return true;
  }

  if (n % 2 === 0) {
    return false;
  }

  for (var i = 3; i < n - 1; i += 2) {
    if (n % i === 0) {
      return false;
    }
  }

  return true;
}
function inRaMangSoNguyenTo() {
  var soNguyenToArr = [];
  if (soNguyenArr) {
    soNguyenArr.forEach((soNguyen) => {
      if (laSoNguyenTo(soNguyen)) {
        soNguyenToArr.push(soNguyen);
      }
    });
    document.getElementById(
      "inSoNguyenTo"
    ).innerHTML = `Số nguyên tố là : ${soNguyenToArr}`;
    return;
  }
}
// Bài 3:
function tinhS() {
  var n = document.getElementById("txtNhapSoLan1").value * 1;
  document.getElementById("txtNhapSoLan1").value = "";
  var sum = 0;
  for (var i = 2; i <= n; i++) {
    sum += i;
  }
  sum += 2 * n;
  document.getElementById("result3").innerHTML = `n = ${sum}`;
}
// Bài 4:
function tinhSoLuongUocSo() {
  var n = document.getElementById("txtNhapSoLan2").value * 1;
  var uocSoArr = [];
  // console.log('n: ', n);
  document.getElementById("txtNhapSoLan2").value = "";
  for (var i = 1; i <= n; i++) {
    if (n % i == 0) {
      uocSoArr.push(i);
    }
  }
  document.getElementById(
    "result4"
  ).innerHTML = `Số ${n} có ${uocSoArr.length} ước số bao gồm: ${uocSoArr}`;
}
// Bài 5:
function timSoDaoNguoc() {
  var n = document.getElementById("txtNhapSoLan3").value * 1;
  document.getElementById("txtNhapSoLan3").value = "";
  var myString = n.toString();
  var newString = "";
  for (var i = myString.length - 1; i >= 0; i--) {
    newString += myString[i];
  }
  newString = newString * 1;
  document.getElementById("result5").innerHTML = `Số đảo ngược: ${newString}`;
}
// Bài 6:
function timSoNguyenDuongLonNhat() {
  var sum = 0;
  var result = 0;
  var timX = document.getElementById("result6");
  for (var i = 1; sum <= 100; i++) {
    sum += i;
    result = i;
  }
  timX.innerHTML = `x là: ${result}`;
}
// bài 7
function inBangCuuChuong() {
  var n = document.getElementById("txtNhapSoLan4").value * 1;
  document.getElementById("txtNhapSoLan4").value = "";
  var rows = "";
  for (var i = 0; i <= 10; i++) {
    var row = `
      <div>
      ${n} x ${i} = ${n * i};
      </div>
      `;
    rows += row;
  }
  document.getElementById("result7").innerHTML = rows;
}
// bài 8
function chiaBai() {
  var players = [[], [], [], []];
  var cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];
  for (var i = 0; i < players.length; i++) {
    for (var j = 0; j < cards.length; j += 4) {
      players[i].push(cards[j + i]);
    }
  }
  var rows = "";
  for (var i = 0; i < players.length; i++) {
    var row = `
      <div>
     người chơi thứ ${i + 1}: ${players[i]}    
      </div>
      `;
    rows += row;
  }
  document.getElementById("result8").innerHTML = rows;
}
// bài 9
function timSoGaSoCho() {
  var tongSoCon = document.getElementById("txtSoCon").value * 1;
  var tongSoChan = document.getElementById("txtSoChan").value * 1;

  var soCho = (tongSoChan - tongSoCon * 2) / 2;
  var soGa = tongSoCon - soCho;
  var result = document.getElementById("result9");
  result.innerHTML = `Số con chó là: ${soCho}, Số con gà là: ${soGa}`;
}
// Bài 10: tính góc đồng hồ
